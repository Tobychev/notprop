import numpy as np
#eLARS1 
#Halpa = 4562.77686
#NII = 1993.29504

c        = 29979245800.    # speed of light in cgs
AB_ZPT   = 48.6            # AB mag zeropoint 

def flam2ab(flam,lam,dflam=None):
	"""
		converts erg/s/cm2/AA to AB mags
	"""
	if dflam == None:
		fnu = flam2fnu(flam, lam)
		ab  = fnu2ab(fnu)
		return ab
	else: 
		fnu,dfnu = flam2fnu(flam, lam, dflam=dflam)
		ab,dab   = fnu2ab(fnu,dfnu)
		return ab,dab 

def flam2fnu (flam,lam,dflam=None):
	"""
		converts /AA to /Hz 
	"""
	fnu=flam/(c*1.e8)*lam**2
	if dflam==None:
		return fnu
	else:
		dfnu = dflam/(c*1.e8)*lam**2
		return fnu,dfnu

def fnu2ab(fnu, dfnu=None):
	"""
		converts erg/s/cm2/Hz to AB mags
	"""
	ab = -2.5*np.log10(fnu)-AB_ZPT
	if dfnu == None:
		return ab
	else: 
		dab = 0.434 * 2.5 * dfnu/fnu
		return ab, dab

def linemag(lineflux,contflux,contmag):
	return contmag-2.5*np.log10(1+ lineflux/contflux)

def surfmag(r,mag):
	return mag + 2.5*np.log10(np.pi*r**2)
#eLARS1 SDSS flux at OII 3727 line
el1_fwhm =2*2.329256
el1_cont =  231.743896
el1_ew =  61.115623
OII = 3727
line_flux = 1e-17*el1_cont*el1_ew/el1_fwhm
el1_OII = flam2ab(line_flux,OII)

#eLARS1 SDSS flux at OIII 4363 line
el1_fwhm =2*2.560282
el1_cont = 197.843842
el1_ew = 0.514001
OIII = 4363
line_flux = 1e-17*el1_cont*el1_ew/el1_fwhm
print "OIII"+str(OIII), line_flux
el1_OIII = flam2ab(line_flux,OIII)

#eLARS1 SDSS flux at OIII 5007 line
el1_fwhm =2*1.884216
el1_cont = 173.003632
el1_ew =5.804216 
OIII = 5007
line_flux = 1e-17*el1_cont*el1_ew/el1_fwhm
print "OIII"+str(OIII), line_flux
el1_OIII5 = flam2ab(line_flux,OIII)


#eLARS1 SDSS flux at SII 6720 line
el1_fwhm =2*2.570151
el1_cont = 137.385788 
el1_ew = 23.149961
SII = 6720
line_flux = 1e-17*el1_cont*el1_ew/el1_fwhm
print "SII"+str(SII), line_flux
el1_SII = flam2ab(line_flux,SII)

 
print "OII  3727 line mag:", el1_OII  #ETC@3.6 ks = 17.08 
print "OIII 4365 line mag:", el1_OIII #ETC@3.6 ks = 17.08 
print "OIII 5007 line mag:", el1_OIII5 #ETC@3.6 ks = 85.61 
print "SII  6720 line mag:", el1_SII  #ETC@3.6 ks = 126.17

